### Construindo as imagens
  * docker build -t mysql-image -f api/db/Dockerfile .
  * docker build -t node-image -f api/Dockerfile .

### Rodando os containers
  * docker run -d -v $(pwd)/api/db/data:/var/lib/mysql --rm --name mysql-container mysql-image
  * docker run -d -v $(pwd)/api:/home/node/app -p 9001:9001 --link mysql-container --rm --name node-container node-image

### Agora faça o restore do banco:
  * docker exec -i mysql-container mysql -u root -p cumbuca < api/db/script.sql
const knex = require('../database');

module.exports = {
  async index(req, res){
    try{
      const products = await knex.select('*').from('products');

      return res.json({response: products});
    } catch (error) {
      return res.json(error.message)
    }
  }
}
const express = require('express');
const DashboardController = require('./controller/DashboardController');

const routes = express.Router();

routes.get('/dashboard', DashboardController.index);

module.exports = routes;